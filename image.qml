import QtQuick 2.5

BaseDelegate {
    Image {
        id: itemImage

        source: model.src

        width: parent.width
        fillMode: Image.PreserveAspectFit
        horizontalAlignment: Image.AlignLeft
    }
}
