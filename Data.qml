import QtQuick 2.5

ListModel {
    id: listModel

    ListElement {
        text: "Hello World"
        type: "text"
        src: ""
    }
    ListElement {
        text: "https://www.kde.org"
        type: "link"
        src: ""
    }
    ListElement {
        text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
        type: "text"
        src: ""
    }
    ListElement {
        text: "<b>Bold</b>, <i>italicized</i>, <u>you're out!</u>"
        type: "text"
        src: ""
    }
    ListElement {
        text: ""
        type: "image"
        src: "file:///usr/share/plasma/look-and-feel/org.kde.oxygen/contents/splash/images/kde.png"
    }
    ListElement {
        text: "https://www.qt.io/"
        type: "link"
        src: ""
    }
    ListElement {
        text: ""
        type: "image"
        src: "https://c.slashgear.com/framework/asset/slashgear_logo.png"
   }

}
