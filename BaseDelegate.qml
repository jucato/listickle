import QtQuick 2.5

Rectangle {
    color: parent.ListView.isCurrentItem ? "white" : "lightsteelblue"
    width: parent.width
    height: childrenRect.height
    border.color: "black"

    focus: true

    MouseArea {
        anchors.fill: parent

        onClicked: {
            listView.currentIndex = index;
        }

        onDoubleClicked: {
            editItem(listView.currentIndex);
        }
    }

    Keys.onPressed: {
        if (event.key == Qt.Key_Delete) {
            editToolbox.deleteItem(listView.currentIndex);
        } else if (event.key == Qt.Key_Return) {
            editItem(listView.currentIndex);
        }
    }

    function editItem(index) {
        var modelIndex = listModel.get(index);

        itemEditorDialog.itemID.text = modelIndex.id;
        itemEditorDialog.itemType.text = modelIndex.type;
        itemEditorDialog.itemData.text = modelIndex.text;
        itemEditorDialog.itemCreated.text =  new Date(model.created).toLocaleString();
        itemEditorDialog.itemModified.text = new Date(model.modified).toLocaleString();
        itemEditorDialog.visible = true;
        itemEditorDialog.itemData.focus = true;
        itemEditorDialog.itemData.cursorPosition = itemEditorDialog.itemData.text.length;
    }
}
