import QtQuick 2.5

BaseDelegate {
    Text {
        id: itemText
        width: parent.width
        height: contentHeight
        wrapMode: Text.WordWrap
        text: model.text
    }
}
