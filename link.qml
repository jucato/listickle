import QtQuick 2.5

BaseDelegate {
    Text {
        id: itemLink
        width: parent.width
        height: contentHeight
        wrapMode: Text.WordWrap
        text: "<a href=\"" + model.text + "\">" + model.text + "</a>"

        onLinkActivated: Qt.openUrlExternally(model.text)
    }
}
