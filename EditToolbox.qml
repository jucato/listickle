import QtQuick 2.5
import QtQuick.Layouts 1.2
import QtQuick.Dialogs 1.2
import QtQuick.LocalStorage 2.0

Rectangle {
    anchors.left: parent.left; anchors.right: parent.right; anchors.bottom: parent.bottom
    height: 64 // MAGIC NUMBER
    color: "lightgray"

    RowLayout {
        id: textInputRow
        height: 32
        anchors.left: parent.left; anchors.right: parent.right; anchors.top: parent.top
        anchors.bottom:  editButtons.top

        Text {
            height: parent.height
            text: "Idx: " + listView.currentIndex + " | RID: " + listModel.get(listView.currentIndex).id
        }

        Rectangle {
            id: deleteButton
            color: "red"
            height: parent.height
            width: 64

            Text {
                anchors.centerIn: parent
                text: "Delete"
            }

            MouseArea {
                anchors.fill: parent
                onClicked: deleteItem(listView.currentIndex);
            }
        }

        Rectangle {
            id: openButton
            color: "cyan"
            height: parent.height
            width: 64

            Text {
                anchors.centerIn: parent
                text: "Open"
            }

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    fileDialog.visible = true
                }
            }
        }

        Rectangle {
            id: addButton
            color: "green"
            height: parent.height
            width: 64

            Text {
                anchors.centerIn: parent
                text: "Add"
            }

            MouseArea {
                anchors.fill: parent
                onClicked:
                {
                    itemEditorDialog.itemID.text = listModel.count;
                    itemEditorDialog.itemType.text = "text";
                    itemEditorDialog.itemData.text = "Text Here"
                    itemEditorDialog.itemCreated.text = "Now";
                    itemEditorDialog.itemModified.text = "N/A";
                    itemEditorDialog.visible = true;
                    itemEditorDialog.itemData.focus = true;
                    itemEditorDialog.itemData.cursorPosition = itemEditorDialog.itemData.text.length;
                }
            }
        }
    }


    RowLayout {
        id: editButtons
        anchors.left: parent.left; anchors.right: parent.right; anchors.bottom: parent.bottom
        anchors.top:  textInputRow.bottom
        height: 32

        Rectangle {
            id: moveButton
            color: "pink"
            height: parent.height
            width: 64

            Text {
                anchors.centerIn: parent
                text: "Move to"
            }

            MouseArea {
                anchors.fill: parent
                onClicked: moveItem(listView.currentIndex, parseInt(moveTextInput.text));
            }
        }

        TextInput {
            id: moveTextInput
            height: parent.height
            width: 24
            text: "#"
        }

        Rectangle {
            id: clearButton
            color: "pink"
            height: parent.height
            width: 64

            Text {
                anchors.centerIn: parent
                text: "Clear"
            }

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    var db = LocalStorage.openDatabaseSync("listickle.db", "1.0", "Listickle Database", 1000000);

                    db.transaction(
                        function(tx) {
                            tx.executeSql('DROP TABLE listickle');
                            tx.executeSql('CREATE TABLE IF NOT EXISTS listickle(id INTEGER PRIMARY KEY AUTOINCREMENT, text TEXT, type TEXT, src TEXT, created INTEGER, modified INTEGER)');
                        }
                    )

                    listModel.clear();
                }
            }
        }

    }

    function addItem(text, type, src, created) {
        var db = LocalStorage.openDatabaseSync("listickle.db", "1.0", "Listickle Database", 1000000);

        db.transaction(
            function(tx) {
                var res = tx.executeSql('INSERT INTO listickle(text, type, src, created, modified) VALUES(?, ?, ?, ?, ?)', [text, type, src, created, created]);
                listModel.append({"id": parseInt(res.insertId), "text" : text, "type": type, "src": src, "created": created, "modified": created});
            }
        )

        listView.currentIndex = listModel.count - 1;
    }

    function moveItem(fromIndex, toIndex) {
        var db = LocalStorage.openDatabaseSync("listickle.db", "1.0", "Listickle Database", 1000000);
        var success = false;

        db.transaction(
            function(tx) {
                if (toIndex > fromIndex) {
                    // Give row to be moved a temporary row ID
                    tx.executeSql("UPDATE listickle SET id = -1 WHERE id = ? ", [fromIndex]);
                    listModel.get(fromIndex).id = -1;

                    // Shift rows up
                    for(var i = fromIndex; i < toIndex; i++) {
                        tx.executeSql("UPDATE listickle SET id = ? WHERE id = ? ", [i, (i + 1)]);
                        listModel.get(i + 1).id = i;
                    }
                    // Give row to be moved its final row ID
                    tx.executeSql("UPDATE listickle SET id = ? WHERE id = -1 ", [toIndex]);
                    listModel.get(fromIndex).id = toIndex;

                    success = true;
                } else if (fromIndex > toIndex) {
                    // Give row to be moved a temporary row ID
                    tx.executeSql("UPDATE listickle SET id = -1 WHERE id = ? ", [fromIndex]);
                    listModel.get(fromIndex).id = -1;

                    // Shift rows down
                    for(var i = fromIndex; i > toIndex; i--) {
                        tx.executeSql("UPDATE listickle SET id = ? WHERE id = ? ", [i, (i - 1)]);
                        listModel.get(i - 1).id = i;
                    }

                    // Give row to be moved its final row ID
                    tx.executeSql("UPDATE listickle SET id = ? WHERE id = -1 ", [toIndex]);
                    listModel.get(fromIndex).id = toIndex;

                    success = true;
                }
            }
        )

        // Only move model if db transactions succeeded
        if (success) {
            listModel.move(listView.currentIndex, moveTextInput.text, 1)
            listView.positionViewAtIndex(parseInt(moveTextInput.text), ListView.Visible );
        }
    }

    function deleteItem(index) {
        var db = LocalStorage.openDatabaseSync("listickle.db", "1.0", "Listickle Database", 1000000);

        db.transaction(
            function(tx) {
                var res = tx.executeSql('DELETE FROM listickle WHERE created = ?', [listModel.get(index).created]);
                if (res.rowsAffected === 1) {
                    // Shift rows up
                    for(var i = index; i < listModel.count - 1; i++) {
                        tx.executeSql("UPDATE listickle SET id = ? WHERE id = ? ", [i, (i + 1)]);
                        listModel.get(i + 1).id = i;
                    }

                    listModel.remove(index, 1);
                }
            }
        )
    }

    FileDialog {
        id: fileDialog
        title: "Please choose an image file"
        folder: shortcuts.pictures
        nameFilters: ["Image files (*.jpg *.png *.bmp)"]

        onAccepted: {
            addItem("", "image", Qt.resolvedUrl(fileDialog.fileUrl));
        }
        onRejected: {
        }
    }

}
