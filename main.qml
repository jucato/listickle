import QtQuick 2.5
import QtQuick.Window 2.0
import QtQuick.LocalStorage 2.0
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.1

Window {
    visible: true
    width: 320
    height: 480
    title: qsTr("Listickle")

    ListView {
        id: listView
        anchors.left: parent.left; anchors.right: parent.right; anchors.top: parent.top
        anchors.bottom: editToolbox.top

        model: listModel

        focus: true

        delegate: Loader {
            id: itemLoader
            width: parent.width

            source: model.type + ".qml"
        }

        function openDb() {
            var db = LocalStorage.openDatabaseSync("listickle.db", "1.0", "Listickle Database", 1000000);

            db.transaction(
                function(tx) {
                    // Create the database if it doesn't already exist
                    tx.executeSql('CREATE TABLE IF NOT EXISTS listickle(id INTEGER UNIQUE, text TEXT, type TEXT, src TEXT, created INTEGER PRIMARY KEY UNIQUE NOT NULL, modified INTEGER)');

                    var results = tx.executeSql('SELECT COUNT(*) AS cnt FROM listickle');
                    if (results.rows.item(0).cnt === 0) {
                        console.log("Table is empty. Populating table ...");
                        tx.executeSql('INSERT INTO listickle(id, text, type, src, created, modified) VALUES(?, ?, ?, ?, ?, ?)', [0, "Hello World", "text", "", 1494941943641, 1494941943641]);
                        tx.executeSql('INSERT INTO listickle(id, text, type, src, created, modified) VALUES(?, ?, ?, ?, ?, ?)', [1, "https://www.kde.org", "link", "", 1494941727285, 1494941727285]);
                        tx.executeSql('INSERT INTO listickle(id, text, type, src, created, modified) VALUES(?, ?, ?, ?, ?, ?)', [2, "", "image", "androidcommunity.png", 1494942022767, 1494942022766])
                        tx.executeSql('INSERT INTO listickle(id, text, type, src, created, modified) VALUES(?, ?, ?, ?, ?, ?)', [3, "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.", "text", "", 1494941862875, 1494941862875]);
                        tx.executeSql('INSERT INTO listickle(id, text, type, src, created, modified) VALUES(?, ?, ?, ?, ?, ?)', [4, "<b>Bold</b>, <i>italicized</i>, <u>you're out!</u>", "text", "", 1259271052145, 1259271052145]);
                        tx.executeSql('INSERT INTO listickle(id, text, type, src, created, modified) VALUES(?, ?, ?, ?, ?, ?)', [5, "", "image", "kde.png", 1494941984123, 1494941984123]);
                        tx.executeSql('INSERT INTO listickle(id, text, type, src, created, modified) VALUES(?, ?, ?, ?, ?, ?)', [7, "https://www.qt.io/", "link", "", 1493271031453, 1493271031453]);
                        tx.executeSql('INSERT INTO listickle(id, text, type, src, created, modified) VALUES(?, ?, ?, ?, ?, ?)', [8, "", "image", "slashgear.png", 1494942022766, 1494942022766])
                    }

                    results = tx.executeSql('SELECT id, text, type, src, created, modified FROM listickle ORDER BY id ASC');
                    for (var i = 0; i < results.rows.length; i++) {
                        listModel.append({"id": results.rows.item(i).id,
                                          "text": results.rows.item(i).text,
                                          "type": results.rows.item(i).type,
                                          "src": results.rows.item(i).src,
                                          "created": results.rows.item(i).created,
                                          "modified": results.rows.item(i).modified});
                    }
                }
            )
        }

        Component.onCompleted: {
            openDb();
            currentIndex = count - 1; // TODO: Find a better way to scroll to bottom on startup
        }

    }

    EditToolbox {
        id: editToolbox
    }

    ListModel { id: listModel }

    Dialog {
        id: itemEditorDialog
        visible: false
        title: "Item Editor"
        standardButtons: StandardButton.Cancel | StandardButton.Save

        property alias itemID: itemIDTextEdit;
        property alias itemType: itemTypeText;
        property alias itemData: itemDataTextEdit;
        property alias itemCreated: itemDateCreatedText
        property alias itemModified: itemDateModifiedText

        GridLayout {
            id: gridLayout
            columns: 2

            Text { text: "ID:"; font.bold: true; }
            TextEdit  {
                id: itemIDTextEdit
                text: "ID";
                Layout.fillWidth: true;
                enabled: false;
            }

            Text { text: "Type: "; font.bold: true; }
            Text {
                id: itemTypeText
                text: "text";
            }


            Text { text: "Data:"; font.bold: true; }
            TextEdit  {
                id: itemDataTextEdit
                text: "Text here";
                Layout.fillWidth: true;
                enabled: true;
            }

            Text { text: "Created:"; font.bold: true; }
            Text {
                id: itemDateCreatedText
                Layout.fillWidth: true;
            }

            Text { text: "Modified:"; font.bold: true; }
            Text {
                id: itemDateModifiedText
                Layout.fillWidth: true;
            }
        }

        onAccepted: {
            if (parseInt(itemID.text) === listModel.count) {
                editToolbox.addItem(itemData.text, itemType.text, "", Date.now());
            } else {
                var db = LocalStorage.openDatabaseSync("listickle.db", "1.0", "Listickle Database", 1000000);
                var timestamp = Date.now();

                db.transaction(
                    function(tx) {
                        var results = tx.executeSql('UPDATE listickle SET text = ?, type = ?, src = ?, modified = ? WHERE created = ?', [itemData.text, itemType.text, "", timestamp, listModel.get(listView.currentIndex).created]);
                    }
                )

                listModel.setProperty(listView.currentIndex, "text", itemData.text);
                listModel.setProperty(listView.currentIndex, "type", itemType.text);
                listModel.setProperty(listView.currentIndex, "modified", timestamp);
            }
        }

        onRejected: {
            itemID.text = "";
            itemType.text = "text";
            itemData.text = ""
        }
    }
}
